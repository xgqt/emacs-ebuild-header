;;; ebuild-header.el --- Ebuild mode header line -*- lexical-binding: t -*-


;; This file is part of emacs-ebuild-header - Ebuild mode header line.
;; Cpyright (c) 2019-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License

;; emacs-ebuild-header is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; emacs-ebuild-header is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-ebuild-header.  If not, see <https://www.gnu.org/licenses/>.


;; Authors: Maciej Barć <xgqt@riseup.net>
;; Created: 21 Jul 2022
;; Version: 0.0.0
;; Keywords: convenience languages
;; Homepage: https://gitlab.com/xgqt/emacs-ebuild-header
;; Package-Requires: ((emacs "24.3"))
;; SPDX-License-Identifier: GPL-2.0-or-later



;;; Commentary:


;; Ebuild mode header line.



;;; Code:


(defcustom ebuild-header-enable t
  "When true display buffer header when in ebuild-mode."
  :type 'boolean
  :group 'ebuild)


(defun ebuild-header--repo-root (file-path)
  "Return repo root with base of FILE-PATH."
  (locate-dominating-file file-path "profiles/repo_name"))

(defun ebuild-header--read-repo-name-file (file-path)
  "Read contents of FILE-PATH.
Return a string."
  (with-temp-buffer
    (insert-file-contents file-path)
    (replace-regexp-in-string "\n" "" (buffer-string))))

(defun ebuild-header--make-header ()
  "Create ebuild buffer header."
  (let* ((file-path
          (buffer-file-name))
         (repo-root
          (ebuild-header--repo-root file-path))
         (path-list
          (reverse (split-string (replace-regexp-in-string
                                  (concat "^" repo-root "/") "" file-path)
                                 "/")))
         (eclassp
          (string-match ".*/eclass/.*\\.eclass$" file-path)))
    (when (and repo-root (>= (length path-list) 3))
      (let ((repo-file-contents
             (ebuild-header--read-repo-name-file
              (expand-file-name "profiles/repo_name" repo-root))))
        (cond
         (eclassp
          (let* ((eclass-name
                  (nth 0 path-list))
                 (eclass
                  (replace-regexp-in-string "\\.eclass" "" eclass-name)))
            (concat " "
                    (propertize repo-file-contents
                                'face
                                '(:foreground "pink" :weight bold))
                    (propertize " :: "
                                'face
                                '(:foreground "purple"))
                    (propertize "eclass"
                                'face
                                '(:foreground "lightblue" :weight bold))
                    (propertize " / "
                                'face
                                '(:foreground "purple"))
                    (propertize eclass
                                'face
                                '(:foreground "lightgreen" :weight bold)))))
         (t
          (let* ((category (nth 2 path-list))
                 (package-name (nth 1 path-list))
                 (package-ebuild (nth 0 path-list))
                 (package
                  (replace-regexp-in-string "\\.ebuild" "" package-ebuild))
                 (package-version
                  (replace-regexp-in-string (concat package-name "-")
                                            ""
                                            package)))
            (concat " "
                    (propertize repo-file-contents
                                'face
                                '(:foreground "pink" :weight bold))
                    (propertize " :: "
                                'face
                                '(:foreground "purple"))
                    (propertize category
                                'face
                                '(:foreground "lightblue" :weight bold))
                    (propertize " / "
                                'face
                                '(:foreground "purple"))
                    (propertize package-name
                                'face
                                '(:foreground "lightgreen" :weight bold))
                    (propertize " - "
                                'face
                                '(:foreground "purple"))
                    (propertize package-version
                                'face
                                '(:foreground "white" :weight bold))))))))))


;;;###autoload
(defun ebuild-header-set-header-line-format ()
  "Set ebuild buffer header."
  (when (and ebuild-header-enable buffer-file-name)
    (setq-local header-line-format
                '("" (:eval (ebuild-header--make-header))))))

;;;###autoload
(add-hook 'ebuild-mode-hook 'ebuild-header-set-header-line-format)


(provide 'ebuild-header)



;;; ebuild-header.el ends here
